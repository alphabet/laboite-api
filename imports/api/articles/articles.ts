/* eslint-disable func-names */
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { getLabel } from '../../utils/utils';
import { ArticleType } from '/imports/types/collections';
import RegEx from '/imports/utils/regExp';

const Articles: Mongo.Collection<ArticleType> = new Mongo.Collection('articles');

// Deny all client-side updates since we will be using methods to manage this collection
Articles.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Articles.schema = new SimpleSchema(
  {
    title: {
      type: String,
      min: 1,
      label: getLabel('api.articles.labels.title'),
    },
    slug: {
      type: String,
      min: 1,
      label: getLabel('api.articles.labels.slug'),
    },
    userId: {
      type: String,
      label: getLabel('api.articles.labels.userId'),
    },
    draft: {
      type: Boolean,
      label: getLabel('api.articles.labels.draft'),
      defaultValue: false,
      optional: true,
    },
    structure: {
      type: RegEx.Id,
      label: getLabel('api.articles.labels.structure'),
    },
    markdown: {
      type: Boolean,
      label: getLabel('api.articles.labels.markdown'),
      defaultValue: false,
    },
    content: { type: String, label: getLabel('api.articles.labels.content'), min: 1 },
    description: { type: String, label: getLabel('api.articles.labels.description'), max: 400 },
    tags: { type: Array, defaultValue: [], label: getLabel('api.articles.labels.tag') },
    'tags.$': { type: String, min: 1 },
    groups: {
      type: Array,
      defaultValue: [],
      label: getLabel('api.articles.labels.groups'),
    },
    'groups.$': { type: Object },
    'groups.$._id': { type: String, regEx: RegEx.Id },
    'groups.$.name': { type: String },
    'groups.$.type': { type: SimpleSchema.Integer },
    createdAt: {
      type: Date,
      label: getLabel('api.articles.labels.createdAt'),
      optional: true,
    },
    updatedAt: {
      type: Date,
      label: getLabel('api.articles.labels.updatedAt'),
    },
    visits: {
      type: SimpleSchema.Integer,
      defaultValue: 0,
    },
    licence: {
      type: String,
      defaultValue: '',
    },
  },
  { clean: { removeEmptyStrings: false } },
);

Articles.publicFields = {
  title: 1,
  slug: 1,
  userId: 1,
  draft: 1,
  content: 1,
  createdAt: 1,
  updatedAt: 1,
  description: 1,
  groups: 1,
  markdown: 1,
  visits: 1,
  tags: 1,
  structure: 1,
  licence: 1,
};

Articles.attachSchema(Articles.schema);

export default Articles;
