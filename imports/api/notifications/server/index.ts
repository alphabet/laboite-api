import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import { Roles } from 'meteor/alanning:roles';
import Notifications from '../notifications';
import Groups from '../../groups/groups';
import { addExpiration } from '../methods';
import { createGroupNotification } from './notifsutils';
import {
  forbidden403,
  notFound404,
  success200,
  unauthorized401,
  unsupported415,
} from '../../config/responses';
import { GroupType } from '/imports/types/collections';
import { PostNotifContentType } from '/imports/types/methods';
import { RequestFunctionType } from '/imports/types/rest';

export const postNotification = (content: PostNotifContentType): any => {
  // POST api/notifications
  if (!!content && 'userId' in content) {
    // Single user notification
    // find user by _id, username or email
    const userData = addExpiration(content);
    let user: Meteor.User | null | undefined;
    if (content.userId) {
      user = Meteor.users.findOne({
        $or: [
          { _id: userData.userId },
          { 'emails.address': userData.userId },
          { username: userData.userId },
        ],
      });
    }
    // check that user exists
    if (!user) {
      return notFound404(i18n.__('api.groups.unknownUser'));
    } else {
      userData.userId = user._id;
    }
    const notifId = Notifications.insert(userData);

    return success200(`Notification ${notifId} sent to user `, notifId);
  }
  if (!!content && 'groupId' in content) {
    // Group notification
    const group: GroupType | undefined = Groups.findOne(
      { _id: content.groupId },
      { fields: Groups.adminFields },
    );
    if (!group) {
      return notFound404(i18n.__('api.groups.unknownGroup'));
    }
    createGroupNotification(
      undefined,
      content.groupId,
      content.title,
      content.content,
      content.link || '',
    );
    return success200(`Group Notification for ${group.name} sent by API`);
  }
  return unsupported415(i18n.__('Notification sent by API with neither userId nor groupId'));
};

export const findUsersNotifications: RequestFunctionType = async (
  request,
  _params,
  _pathname,
  _path,
  sender,
) => {
  const {
    params: { userId },
  } = request as any;
  if (!!sender && sender._id !== userId && !Roles.userIsInRole(sender._id, 'admin')) {
    return forbidden403("You cannot read someone else's notifications");
  }
  const notifications = Notifications.find({ userId }, { limit: 30 }).fetch();
  return success200('this is a success', notifications);
};

export const findMyNotifications: RequestFunctionType = async (
  _request,
  _params,
  _pathname,
  _path,
  sender,
) => {
  if (sender) {
    const notifications = Notifications.find({ userId: sender._id }, { limit: 30 }).fetch();
    return success200('this is a success', notifications);
  }
  return unauthorized401();
};
