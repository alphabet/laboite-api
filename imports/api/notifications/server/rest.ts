import { findMyNotifications, findUsersNotifications, postNotification } from '.';
import { RestRouteDefType } from '/imports/types/rest';

export const notificationsRest: RestRouteDefType[] = [
  {
    route: '/notifications/me',
    method: 'get',
    auth: true,
    func: findMyNotifications,
    path: '/notifications/me',
    swagger: {
      get: {
        tags: ['Notifications'],
        summary: "Trouver toutes les notifications de l'utilisateur connecté",
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'findNotificationsMe',
        consumes: ['application/json', 'application/xml'],
        produces: ['application/json', 'application/xml'],
        parameters: [],
        responses: {
          '200': {
            description: 'This is a success',
          },
          '405': {
            description: 'Invalid input',
          },
        },
      },
    },
  },
  {
    route: '/notifications/:userId',
    method: 'get',
    auth: true,
    func: findUsersNotifications,
    path: '/notifications/{userId}',
    swagger: {
      get: {
        tags: ['Notifications'],
        summary: "Trouver toutes les notifications d'un utilisateur",
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'findNotifications',
        consumes: ['application/json', 'application/xml'],
        produces: ['application/json', 'application/xml'],
        parameters: [
          {
            name: 'userId',
            in: 'path',
          },
        ],
        responses: {
          '200': {
            description: 'This is a success',
          },
          '405': {
            description: 'Invalid input',
          },
        },
      },
    },
  },
  {
    route: '/notifications',
    method: 'post',
    auth: true,
    async func(_request, content) {
      return postNotification(content);
    },
    path: '/notifications',
    swagger: {
      post: {
        tags: ['Notifications'],
        summary: 'Créer une notification pour un utilisateur ou un groupe',
        description:
          "Pour envoyer à un utilisateur, utilisez soit son email, son ID, ou encore son username (enlever les champs inutiles). Pour un groupe d'utilisateur, indiquez le groupeID. ",
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'createNotifications',
        consumes: ['application/json', 'application/xml'],
        produces: ['application/json', 'application/xml'],
        parameters: [
          {
            in: 'body',
            name: 'body',
            description: 'Notification data to create in the database',
            required: true,
            schema: {
              $ref: '#/definitions/Notification',
            },
          },
        ],
        responses: {
          '200': {
            description: 'This is a success',
          },
          '405': {
            description: 'Invalid input',
          },
        },
      },
    },
  },
];
