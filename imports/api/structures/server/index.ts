import { Roles } from 'meteor/alanning:roles';
import { Meteor } from 'meteor/meteor';
import { forbidden403, notFound404, success200 } from '../../config/responses';
import Structures from '../structures';
import { UserType } from '/imports/types/collections';
import { RequestFunctionType } from '/imports/types/rest';

export const getUserStructure: RequestFunctionType = async (
  request,
  _params,
  _pathname,
  _path,
  sender,
) => {
  const {
    params: { userId },
  } = request as any;
  if (!!sender && sender._id !== userId && !Roles.userIsInRole(sender._id, 'admin')) {
    return forbidden403("You cannot read someone else's structures");
  }
  const user = Meteor.users.findOne({ _id: userId });
  if (user) {
    const { structure } = user as UserType;
    const userStructure = Structures.findOne({ _id: structure });
    return success200('this is a success', userStructure);
  }
  return notFound404('Utilisateur non trouvé');
};

export const getMyStructure: RequestFunctionType = async (
  _request,
  _params,
  _pathname,
  _path,
  sender,
) => {
  if (sender) {
    const { structure } = sender as UserType;
    const userStructure = Structures.findOne({ _id: structure });
    return success200('this is a success', userStructure);
  }
  return notFound404('Utilisateur non trouvé');
};
