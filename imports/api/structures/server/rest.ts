import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { forbidden403, success200 } from '../../config/responses';
import Structures from '../structures';
import { RestRouteDefType } from '/imports/types/rest';

async function getStructures(sender: any) {
  if (!!sender && !Roles.userIsInRole(sender._id, 'admin')) {
    return forbidden403('You must be admin to access statistics');
  }
  const byStruct: any[] = await Meteor.users
    .rawCollection()
    .aggregate([{ $group: { _id: '$structure', count: { $sum: 1 } } }])
    .toArray();
  const allStruct: any = {};
  // fetch structure infos
  Structures.find({})
    .fetch()
    .forEach((entry) => {
      allStruct[entry._id] = {
        id: entry._id,
        parentId: entry.parentId ? entry.parentId : '',
        name: entry.name,
        users: 0,
      };
    });
  // add user count
  byStruct.forEach((countEntry) => {
    if (countEntry._id) {
      allStruct[countEntry._id] = { ...allStruct[countEntry._id], users: countEntry.count };
    } else {
      allStruct[countEntry._id] = {
        id: null,
        parentId: null,
        name: 'Unassigned users',
        users: countEntry.count,
      };
    }
  });
  const finalRes = Object.values(allStruct);
  return success200('this is a success', finalRes);
}

export const structuresRest: RestRouteDefType[] = [
  {
    route: '/statistics/structures',
    method: 'get',
    auth: true,
    async func(_request, _content, _pathname, _path, sender) {
      return getStructures(sender);
    },
    path: '/statistics/structures',
    swagger: {
      get: {
        tags: ['Statistics'],
        summary: 'Statistiques des structures',
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'getStructures',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [],
        responses: {
          '200': {
            description: 'successful operation',
          },
          '401': {
            description: 'Authentication error',
          },
          '403': {
            description: 'Insufficient rights',
          },
        },
      },
    },
  },
];
