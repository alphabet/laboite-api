import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';
import { Picker } from 'meteor/meteorhacks:picker';
import i18n from 'meteor/universe:i18n';
import bodyParser from 'body-parser';
import cors from 'cors';
import Rest from 'connect-rest';

import { groupsRest } from '../groups/server/rest';
import { structuresRest } from '../structures/server/rest';
import { swaggerDoc } from './swagger';
import { unauthorized401 } from './responses';
import { RequestFunctionType, RestMethod, RestRequest } from '/imports/types/rest';
import { usersRest } from '../users/server/rest';

WebApp.connectHandlers.use(bodyParser.urlencoded({ extended: false }));
WebApp.connectHandlers.use(bodyParser.json());
WebApp.connectHandlers.use('*', cors());
WebApp.connectHandlers.use(function (_req, res, next) {
  // add allow origin
  res.setHeader('Access-Control-Allow-Origin', '*');

  // add headers
  res.setHeader(
    'Access-Control-Allow-Headers',
    [
      'Accept',
      'Accept-Charset',
      'Accept-Encoding',
      'Accept-Language',
      'Accept-Datetime',
      'Authorization',
      'Cache-Control',
      'Connection',
      'Cookie',
      'Content-Length',
      'Content-MD5',
      'Content-Type',
      'Date',
      'User-Agent',
      'X-Requested-With',
      'Origin',
      'x-auth-token',
    ].join(', '),
  );
  next();
});

// Initialize REST library
const options = {
  context: '/api',
  apiKey: 'x-auth-token',
};

const rest = Rest.create(options);

// adds connect-rest middleware to connect
WebApp.connectHandlers.use(rest.processRequest());

const protector =
  (func: RequestFunctionType) =>
  (request: RestRequest, content: any, pathname: string, path: string) => {
    try {
      // @ts-ignore: Unreachable code error
      const token = request.headers[options.apiKey];

      if (token) {
        const sender = Meteor.users.findOne({ authToken: token });
        if (sender) {
          console.log(`API used by ${sender.username}`);
          return func(request, content, pathname, path, sender);
        }
        throw new Meteor.Error(
          'api.notifications.removeNotification.notLoggedIn',
          i18n.__('api.notifications.mustBeLoggedIn'),
        );
      } else {
        throw new Meteor.Error(
          'api.notifications.removeNotification.notLoggedIn',
          i18n.__('api.notifications.mustBeLoggedIn'),
        );
      }
    } catch (error: any) {
      return unauthorized401(error.reason);
    }
  };

const routes: RestMethod[] = [...groupsRest, ...structuresRest, ...usersRest];

routes.forEach(
  ({
    method,
    path,
    func,
    auth,
    // roles,
    swagger,
    route,
  }) => {
    swaggerDoc.paths = {
      ...swaggerDoc.paths,
      [path]: { ...swagger },
    };
    if (auth) {
      rest[method]({ path: route }, protector(func));
    } else {
      rest[method]({ path: route }, func);
    }
  },
);

// Get generated swagger config
Picker.route(`${options.context}/swagger.json`, (_params: any, _request: Request, response: any) =>
  response.end(JSON.stringify(swaggerDoc)),
);
