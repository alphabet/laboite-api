import { ResponseAPIType } from '/imports/types/methods';

const httpError = (message: string, code: number) => {
  const error: any = new Error(message);
  error.statusCode = code;
  throw error;
};

export const continue100 = (message = 'No Content'): ResponseAPIType => {
  return {
    code: 100,
    status: 'success',
    message,
  };
};

export const success200 = (message = '', data?: any): ResponseAPIType => {
  return {
    code: 200,
    status: 'success',
    message,
    data,
  };
};

export const success201 = (message = 'Created', data?: any): ResponseAPIType => {
  return {
    code: 201,
    status: 'success',
    message,
    data,
  };
};

export const success205 = (message = 'No Content'): ResponseAPIType => {
  return {
    code: 205,
    status: 'success',
    message,
  };
};

export const badRequest400 = (message = 'Bad Request') => {
  httpError(message, 400);
};

export const unauthorized401 = (message = 'Unauthorized') => {
  httpError(message, 401);
};

export const forbidden403 = (message = 'Forbidden') => {
  httpError(message, 403);
};

export const notFound404 = (message = 'Not Found') => {
  httpError(message, 404);
};

export const notAllowed405 = (message = 'Not Allowed') => {
  httpError(message, 405);
};

export const unsupported415 = (message = 'Unsupported') => {
  httpError(message, 415);
};

export const serverError500 = (message = 'Server Error') => {
  httpError(message, 500);
};

export const tooManyRequests429 = (message = 'Too Many Requests') => {
  httpError(message, 429);
};
