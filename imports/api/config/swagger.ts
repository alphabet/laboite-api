import { Meteor } from 'meteor/meteor';

export const swaggerDoc = {
  swagger: '2.0',
  info: {
    description: '',
    version: '1.0.0',
    title: 'Rizomo/LaBoite REST API',
    license: {
      name: 'EUPL v1.2',
      url: 'https://eupl.eu/1.2/fr/',
    },
  },
  host: Meteor.absoluteUrl().replace('https://', '').replace('http://', ''),
  basePath: 'api',
  tags: [
    // {
    //   name: 'Notifications',
    //   description: 'Méthodes de création et de renvoi de notifications',
    // },
    {
      name: 'Statistics',
      description: 'Méthodes de récupération de statistiques',
    },
  ],
  schemes: ['https', 'http'],
  paths: {},
  securityDefinitions: {
    APIKeyHeader: {
      type: 'apiKey',
      name: 'x-auth-token',
      in: 'header',
    },
  },
  definitions: {
    // Notification: {
    //   type: 'object',
    //   properties: {
    //     userId: {
    //       type: 'string',
    //       format: 'ID',
    //     },
    //     groupId: {
    //       type: 'string',
    //       format: 'ID',
    //     },
    //     title: {
    //       type: 'string',
    //     },
    //     content: {
    //       type: 'string',
    //     },
    //     link: {
    //       type: 'string',
    //     },
    //     type: {
    //       type: 'string',
    //       default: 'info',
    //       enum: ['info'],
    //     },
    //   },
    //   xml: {
    //     name: 'Notification',
    //   },
    // },
    // Structure: {
    //   type: 'object',
    //   properties: {
    //     name: {
    //       type: 'string',
    //     },
    //     parentId: {
    //       type: 'string',
    //       format: 'ID',
    //     },
    //     childrenIds: {
    //       type: 'array',
    //     },
    //     ancestorsIds: {
    //       type: 'array',
    //     },
    //   },
    //   xml: {
    //     name: 'Structure',
    //   },
    // },
    // Service: {
    //   type: 'object',
    //   properties: {
    //     name: {
    //       type: 'string',
    //     },
    //     parentId: {
    //       type: 'string',
    //       format: 'ID',
    //     },
    //     childrenIds: {
    //       type: 'array',
    //     },
    //     ancestorsIds: {
    //       type: 'array',
    //     },
    //   },
    //   xml: {
    //     name: 'Service',
    //   },
    // },
    ApiResponse: {
      type: 'object',
      properties: {
        code: {
          type: 'integer',
          format: 'int32',
        },
        status: {
          type: 'string',
        },
        message: {
          type: 'string',
        },
        data: {
          type: 'string',
        },
      },
    },
  },
  externalDocs: {
    description: 'Find out more about Swagger',
    url: 'http://swagger.io',
  },
};
