import Articles from '../../articles/articles';
import Bookmarks from '../../bookmarks/bookmarks';
import Events from '../../events/events';
import Forms from '../../forms/forms';
import Polls from '../../polls/polls';
import { GroupType } from '/imports/types/collections';

export const getGroupStats = (gr: GroupType, detailed = false) => {
  const allUsers = new Set([...gr.members, ...gr.animators, ...gr.admins]);
  const stats: any = { id: gr._id, name: gr.name, type: gr.type, users: allUsers.size };
  if (detailed) {
    // get number of polls, articles, bookmarks, forms and events
    stats.articles = Articles.find({ groups: { $elemMatch: { _id: gr._id } } }).count();
    stats.bookmarks = Bookmarks.find({ groupId: gr._id }).count();
    stats.events = Events.find({ groups: { $elemMatch: { _id: gr._id } } }).count();
    stats.forms = Forms.find({ groups: gr._id }).count();
    stats.polls = Polls.find({ groups: gr._id }).count();
  }
  return stats;
};

type tgroupTypes = {
  [key: number]: string;
};

export const groupTypes: tgroupTypes = {
  0: 'open',
  5: 'moderated',
  10: 'private',
  15: 'automatic',
};
