import { Roles } from 'meteor/alanning:roles';
import { forbidden403, notFound404, success200 } from '../../config/responses';
import Groups from '../groups';
import { RestRouteDefType } from '/imports/types/rest';
import { getGroupStats, groupTypes } from '.';

export const groupsRest: RestRouteDefType[] = [
  {
    route: '/statistics/groups',
    method: 'get',
    auth: true,
    async func(_request, _content, _pathname, _path, sender) {
      if (!!sender && !Roles.userIsInRole(sender._id, 'admin')) {
        return forbidden403('You must be admin to access statistics');
      }
      const groups = Groups.find({}).fetch();
      const groupResult = groups.map((gr) => getGroupStats(gr));
      return success200('this is a success', groupResult);
    },
    path: '/statistics/groups',
    swagger: {
      get: {
        tags: ['Statistics'],
        summary: 'Statistiques des groupes',
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'getGroupsStatistics',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [],
        responses: {
          '200': {
            description: 'successful operation',
          },
          '401': {
            description: 'Authentication error',
          },
          '403': {
            description: 'Insufficient rights',
          },
        },
      },
    },
  },
  {
    route: '/statistics/groupsbytype',
    method: 'get',
    auth: true,
    async func(_request, _content, _pathname, _path, sender) {
      if (!!sender && !Roles.userIsInRole(sender._id, 'admin')) {
        return forbidden403('You must be admin to access statistics');
      }
      const result: any[] = await Groups.rawCollection()
        .aggregate([{ $group: { _id: '$type', count: { $sum: 1 } } }])
        .toArray();
      const finalRes = result.map((entry) => {
        return {
          type: groupTypes[entry._id],
          count: entry.count,
        };
      });
      return success200('this is a success', finalRes);
    },
    path: '/statistics/groupsbytype',
    swagger: {
      get: {
        tags: ['Statistics'],
        summary: 'Groupes par type',
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'getGroupsByType',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [],
        responses: {
          '200': {
            description: 'successful operation',
          },
          '401': {
            description: 'Authentication error',
          },
          '403': {
            description: 'Insufficient rights',
          },
        },
      },
    },
  },
  {
    route: '/statistics/groups/:groupId',
    method: 'get',
    auth: true,
    async func(_request, _content, _pathname, _path, sender) {
      const { groupId } = _request.parameters;
      const group = Groups.findOne({ _id: groupId });
      if (group === undefined) return notFound404();
      if (!!sender && !Roles.userIsInRole(sender._id, 'admin', groupId)) {
        return forbidden403('You must be group admin to access statistics');
      }
      return success200('this is a success', getGroupStats(group, true));
    },
    path: '/statistics/groups/{groupId}',
    swagger: {
      get: {
        tags: ['Statistics'],
        summary: "Statistiques d'un groupe",
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'getGroupStatistics',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            name: 'groupId',
            in: 'path',
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
          },
          '401': {
            description: 'Authentication error',
          },
          '403': {
            description: 'Insufficient rights',
          },
          '404': {
            description: 'Group not found',
          },
        },
      },
    },
  },
];
