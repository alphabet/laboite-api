import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { getLabel } from '../../utils/utils';
import { EventType } from '/imports/types/collections';
import RegEx from '/imports/utils/regExp';

const Events: Mongo.Collection<EventType> = new Mongo.Collection('events');

// Deny all client-side updates since we will be using methods to manage this collection
Events.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

const settingsGroup = new SimpleSchema({
  _id: {
    type: String,
    min: 1,
  },
  name: {
    type: String,
  },
  type: {
    type: SimpleSchema.Integer,
  },
});

const settingsParticipant = new SimpleSchema({
  _id: {
    type: String,
    min: 1,
  },
  email: {
    type: String,
  },
  status: {
    type: Number,
    allowedValues: [0, 1, 2], // 0: refused - 1: waiting - 2: accepted
    defaultValue: 1,
  },
  groupId: {
    type: String,
    regEx: RegEx.Id,
    optional: true,
  },
});

Events.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: RegEx.Id,
    min: 1,
  },
  title: {
    type: String,
  },
  location: {
    type: String,
    optional: true,
  },
  recurrent: {
    type: Boolean,
    defaultValue: false,
  },
  daysOfWeek: {
    type: Array,
    optional: true,
  },
  'daysOfWeek.$': {
    type: Number,
    optional: true,
  },
  description: {
    type: String,
    optional: true,
  },
  start: {
    type: Date,
  },
  end: {
    type: Date,
  },
  startTime: {
    type: String,
    optional: true,
  },
  endTime: {
    type: String,
    optional: true,
  },
  allDay: {
    type: Boolean,
    defaultValue: false,
  },
  groups: {
    type: Array,
    defaultValue: [],
  },
  'groups.$': {
    type: settingsGroup,
    optional: true,
  },
  participants: {
    type: Array,
    defaultValue: [],
  },
  'participants.$': {
    type: settingsParticipant,
    optional: true,
  },
  guests: {
    type: Array,
    defaultValue: [],
  },
  'guests.$': {
    type: String,
    optional: true,
  },
  userId: {
    type: String,
    regEx: RegEx.Id,
  },
  createdAt: {
    type: Date,
    label: getLabel('api.articles.labels.createdAt'),
    optional: true,
  },
  updatedAt: {
    type: Date,
    label: getLabel('api.articles.labels.updatedAt'),
  },
});

Events.attachSchema(Events.schema);

export default Events;
