import { getAllStructureServices, getStructureServices } from '.';
import { RestRouteDefType } from '/imports/types/rest';

export const servicesRest: RestRouteDefType[] = [
  {
    route: '/services/structure/:structureId',
    method: 'get',
    auth: true,
    func: getStructureServices,
    path: '/services/structure/{structureId}',
    swagger: {
      get: {
        tags: ['Services'],
        summary: "Trouver les services d'une structure",
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'findStructureServices',
        consumes: ['application/json', 'application/xml'],
        produces: ['application/json', 'application/xml'],
        parameters: [
          {
            name: 'structureId',
            in: 'path',
          },
        ],
        responses: {
          '200': {
            description: 'Success',
          },
          '405': {
            description: 'Invalid input',
          },
          '404': {
            description: 'Structure not found',
          },
          '403': {
            description: 'Forbidden',
          },
        },
      },
    },
  },
  {
    route: '/services/structure/:structureId/all',
    method: 'get',
    auth: true,
    func: getAllStructureServices,
    path: '/services/structure/{structureId}/all',
    swagger: {
      get: {
        tags: ['Services'],
        summary: "Trouver tous les services d'une structure et de ses parents",
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'findAllStructureServices',
        consumes: ['application/json', 'application/xml'],
        produces: ['application/json', 'application/xml'],
        parameters: [
          {
            name: 'structureId',
            in: 'path',
          },
        ],
        responses: {
          '200': {
            description: 'Success',
          },
          '405': {
            description: 'Invalid input',
          },
          '404': {
            description: 'Structure not found',
          },
          '403': {
            description: 'Forbidden',
          },
        },
      },
    },
  },
];
