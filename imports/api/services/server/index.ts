import { notFound404, success200 } from '../../config/responses';
import Structures from '../../structures/structures';
import Services from '../services';
import { RequestFunctionType } from '/imports/types/rest';

export const getStructureServices: RequestFunctionType = async (request) => {
  const {
    params: { structureId },
  } = request as any;
  const structure = Structures.findOne({ _id: structureId });
  if (structure) {
    const structureServices = Services.find({ structure: structureId }).fetch();
    return success200('this is a success', structureServices);
  }
  return notFound404('Structure non trouvée');
};

export const getAllStructureServices: RequestFunctionType = async (request) => {
  const {
    params: { structureId },
  } = request as any;
  const structure = Structures.findOne({ _id: structureId });
  if (structure) {
    const { ancestorsIds } = structure;
    const structureServices = Services.find({
      structure: { $in: [structureId, ...ancestorsIds] },
    }).fetch();
    return success200('this is a success', structureServices);
  }
  return notFound404('Structure non trouvée');
};
