import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { getLabel } from '/imports/utils/utils';
import { ServiceType } from '/imports/types/collections';
import RegEx from '/imports/utils/regExp';

const Services = new Mongo.Collection<ServiceType>('services');

// Deny all client-side updates since we will be using methods to manage this collection
Services.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

Services.schema = new SimpleSchema(
  {
    title: {
      type: String,
      min: 1,
      label: getLabel('api.services.labels.title'),
    },
    slug: {
      type: String,
      min: 1,
      label: getLabel('api.services.labels.slug'),
    },
    team: { type: String, label: getLabel('api.services.labels.team') },
    usage: { type: String, label: getLabel('api.services.labels.usage') },
    content: { type: String, label: getLabel('api.services.labels.content') },
    description: {
      type: String,
      max: 80,
      label: getLabel('api.services.labels.description'),
    },
    url: { type: String, label: getLabel('api.services.labels.url') },
    logo: {
      type: String,
      label: getLabel('api.services.labels.logo'),
    },
    state: {
      type: SimpleSchema.Integer,
      allowedValues: [0, 5, 10], // 0 displayed, 5 inactive, 10 invisible
      label: getLabel('api.services.labels.state'),
    },
    categories: {
      type: Array,
      defaultValue: [],
      label: getLabel('api.services.labels.categories'),
    },
    'categories.$': { type: String, regEx: RegEx.Id },
    screenshots: {
      type: Array,
      defaultValue: [],
      label: getLabel('api.services.labels.screenshots'),
    },
    'screenshots.$': { type: String },
    structure: {
      type: RegEx.Id,
      label: getLabel('api.services.labels.structure'),
      defaultValue: '',
    },
    offline: {
      type: Boolean,
      optional: true,
      label: getLabel('api.services.labels.offline'),
    },
  },
  { clean: { removeEmptyStrings: false } },
);

Services.publicFields = {
  title: 1,
  description: 1,
  url: 1,
  logo: 1,
  categories: 1,
  usage: 1,
  team: 1,
  slug: 1,
  state: 1,
  offline: 1,
  structure: 1,
};

Services.allPublicFields = {
  ...Services.publicFields,
  screenshots: 1,
  content: 1,
};

Services.attachSchema(Services.schema);

export default Services;
