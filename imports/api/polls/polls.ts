import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { PollType } from '/imports/types/collections';
import RegEx from '/imports/utils/regExp';

const Polls: Mongo.Collection<PollType> = new Mongo.Collection('polls');

// Deny all client-side updates since we will be using methods to manage this collection
Polls.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

const POLLS_TYPES: any = {
  POLL: 'POLL',
  MEETING: 'MEETING',
};

const SingleDateSchema = new SimpleSchema({
  date: {
    type: Date,
    label: 'Date',
  },
  slots: {
    type: Array,
    label: 'Time Slots',
    optional: true,
  },
  'slots.$': {
    type: String,
  },
});

const SingleMeetingSlotSchema = new SimpleSchema({
  start: {
    type: Date,
    label: 'Start time',
  },
  end: {
    type: Date,
    label: 'End time',
  },
});

Polls.schema = new SimpleSchema(
  {
    _id: {
      type: String,
      regEx: RegEx.Id,
      optional: true,
    },
    title: {
      type: String,
      min: 1,
      label: 'Title',
    },
    userId: {
      type: String,
      regEx: RegEx.Id,
      label: 'Owner',
    },
    description: {
      type: String,
      label: 'Description',
      optional: true,
    },
    duration: {
      type: String,
      label: 'Duration',
      optional: true,
    },
    public: {
      type: Boolean,
      label: 'Public',
      defaultValue: false,
    },
    allDay: {
      type: Boolean,
      label: 'Event all day',
      defaultValue: false,
    },
    active: {
      type: Boolean,
      label: 'Active',
      defaultValue: false,
    },
    completed: {
      type: Boolean,
      label: 'Completed',
      defaultValue: false,
    },
    choosenDate: {
      type: Date,
      label: 'Choosen Date',
      optional: true,
    },
    type: {
      type: String,
      label: 'Type',
      allowedValues: Object.keys(POLLS_TYPES).map((k) => POLLS_TYPES[k]),
      defaultValue: POLLS_TYPES.POLL,
    },
    groups: {
      type: Array,
      label: 'Groups polled',
      defaultValue: [],
    },
    'groups.$': {
      type: String,
      regEx: RegEx.Id,
    },
    dates: {
      type: Array,
      label: 'Dates',
      defaultValue: [],
    },
    'dates.$': {
      type: SingleDateSchema,
    },
    meetingSlots: {
      type: Array,
      label: 'Meeting Time Slots',
      optional: true,
    },
    'meetingSlots.$': {
      type: SingleMeetingSlotSchema,
    },
    createdAt: {
      type: Date,
      label: 'Created date',
    },
    updatedAt: {
      type: Date,
      label: 'Updated date',
    },
  },
  { clean: { removeEmptyStrings: false } },
);

Polls.publicFields = {
  title: 1,
  userId: 1,
  content: 1,
  groups: 1,
  public: 1,
  allDay: 1,
  createdAt: 1,
  updatedAt: 1,
  description: 1,
  dates: 1,
};

Polls.attachSchema(Polls.schema);

export default Polls;
