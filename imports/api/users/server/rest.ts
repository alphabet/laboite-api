import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { forbidden403, success200 } from '../../config/responses';
// import Structures from '../../structures/structures';
import { RestRequest, RestRouteDefType } from '/imports/types/rest';

async function getUsers(request: RestRequest, sender: any) {
  let query: any = {};
  if (request.parameters.structureId) {
    query = { ...query, structure: request.parameters.structureId };
    if (!!sender && !Roles.userIsInRole(sender._id, 'admin', request.parameters.structureId)) {
      return forbidden403('You must be structure admin to access statistics');
    }
  } else if (!!sender && !Roles.userIsInRole(sender._id, 'admin')) {
    return forbidden403('You must be admin to access statistics');
  }
  if (request.parameters.mailDomain) {
    const rxDomain = new RegExp(`^.*${request.parameters.mailDomain}$`);
    query = { ...query, 'emails.address': { $regex: rxDomain } };
  }
  // total number of users
  const totalUsers = Meteor.users.find(query).count();
  // total active users (last login more recent than 1 month)
  const activeLimit = new Date();
  activeLimit.setMonth(activeLimit.getMonth() - 1);
  query = { ...query, lastLogin: { $gt: activeLimit } };
  const activeUsers = Meteor.users.find(query).count();
  const finalRes = { total: totalUsers, active: activeUsers };
  return success200('this is a success', finalRes);
}

async function getUsersMonthly(request: RestRequest, sender: any) {
  let structureQuery = {};
  let numMonths = 12;
  if (request.parameters.structureId) {
    structureQuery = { ...structureQuery, structure: request.parameters.structureId };
    if (!!sender && !Roles.userIsInRole(sender._id, 'admin', request.parameters.structureId)) {
      return forbidden403('You must be structure admin to access statistics');
    }
  } else if (!!sender && !Roles.userIsInRole(sender._id, 'admin')) {
    return forbidden403('You must be admin to access statistics');
  }
  if (request.parameters.months) numMonths = Number(request.parameters.months);
  if (request.parameters.mailDomain) {
    const rxDomain = new RegExp(`^.*${request.parameters.mailDomain}$`);
    structureQuery = { ...structureQuery, 'emails.address': { $regex: rxDomain } };
  }

  // number of users created per month (last 12 months)
  const createdUsers: any[] = [];
  // set date range end to end of current day
  const monthEnd = new Date();
  monthEnd.setHours(23);
  monthEnd.setMinutes(59);
  monthEnd.setSeconds(59);
  // set date range start to start of first day of current month
  const monthStart = new Date();
  monthStart.setDate(1);
  monthStart.setHours(0);
  monthStart.setMinutes(0);
  monthStart.setSeconds(0);
  const monthOffsets = Array(numMonths)
    .fill(0)
    .map((_, i) => i);
  monthOffsets.forEach(() => {
    // number of users created this month
    const numCreated = Meteor.users
      .find({ ...structureQuery, createdAt: { $gte: monthStart, $lte: monthEnd } })
      .count();
    // number of users active this month. We estimate that a user was active if he was created
    // before the end of the month, and if his last known login is after the beginning of the month
    // This means users are considered active all the time between their creation and their last login.
    const numActive = Meteor.users
      .find({ ...structureQuery, createdAt: { $lt: monthEnd }, lastLogin: { $gt: monthStart } })
      .count();
    createdUsers.push({
      month: `${monthStart.getFullYear()}/${monthStart.getMonth() < 9 ? '0' : ''}${
        monthStart.getMonth() + 1
      }`,
      total: numCreated,
      active: numActive,
    });
    // switch date range to previous month
    monthStart.setMonth(monthStart.getMonth() - 1);
    monthEnd.setDate(1);
    // get last day of previous month
    monthEnd.setDate(monthEnd.getDate() - 1);
  });
  return success200('this is a success', createdUsers.reverse());
}

export const usersRest: RestRouteDefType[] = [
  {
    route: '/statistics/users',
    method: 'get',
    auth: true,
    async func(_request, _content, _pathname, _path, sender) {
      return getUsers(_request, sender);
    },
    path: '/statistics/users',
    swagger: {
      get: {
        tags: ['Statistics'],
        summary: 'Statistiques utilisateurs',
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'getUsers',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            in: 'query',
            name: 'structureId',
            description: 'optional structure id',
            required: false,
          },
          {
            in: 'query',
            name: 'mailDomain',
            description: 'optional mail domain',
            required: false,
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
          },
          '401': {
            description: 'Authentication error',
          },
          '403': {
            description: 'Insufficient rights',
          },
        },
      },
    },
  },
  {
    route: '/statistics/monthlyusers',
    method: 'get',
    auth: true,
    async func(_request, _content, _pathname, _path, sender) {
      return getUsersMonthly(_request, sender);
    },
    path: '/statistics/monthlyusers',
    swagger: {
      get: {
        tags: ['Statistics'],
        summary: 'Utilisateurs créés par mois',
        description: '',
        security: [
          {
            APIKeyHeader: [],
          },
        ],
        operationId: 'getUsersMonthly',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            in: 'query',
            name: 'structureId',
            description: 'optional structure id',
            required: false,
          },
          {
            in: 'query',
            name: 'mailDomain',
            description: 'optional mail domain',
            required: false,
          },
          {
            in: 'query',
            name: 'months',
            description: 'optional number of months (default: 12)',
            required: false,
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
          },
          '401': {
            description: 'Authentication error',
          },
          '403': {
            description: 'Insufficient rights',
          },
        },
      },
    },
  },
];
