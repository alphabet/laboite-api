import { Meteor } from 'meteor/meteor';
import SwaggerUI from 'swagger-ui';
import 'swagger-ui/dist/swagger-ui.css';

Meteor.startup(async () => {
  const request = await fetch(`${Meteor.absoluteUrl()}api/swagger.json`);
  const spec = await request.json();
  SwaggerUI({
    spec,
    dom_id: '#target',
    // presets: [SwaggerUI.presets.apis],
  });
  // ui.initOAuth({
  //   appName: 'Swagger UI Webpack Demo',
  //   // See https://demo.identityserver.io/ for configuration details.
  //   clientId: 'implicit',
  // });
});
