import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import SimpleSchema from 'simpl-schema';
import { UserType } from '../types/collections';

export const isActive = (userId: string) => {
  if (!userId) return false;
  const user: Partial<UserType> | undefined = Meteor.users.findOne(userId, {
    fields: { isActive: 1 },
  });
  if (user && user.isActive === true) return true;
  return false;
};

export const getLabel = (i18nLabel: string): any => {
  return () => i18n.__(i18nLabel);
};

export const checkPaginationParams = new SimpleSchema({
  page: { type: SimpleSchema.Integer, defaultValue: 1, label: getLabel('api.methods.labels.page') },
  itemPerPage: {
    type: SimpleSchema.Integer,
    defaultValue: 10,
    label: getLabel('api.methods.labels.pageSize'),
  },
  search: { type: String, defaultValue: '', label: getLabel('api.methods.labels.filter') },
});

export function genRandomPassword(pwdlen = 16) {
  // original code and explanations here :
  // https://www.geeksforgeeks.org/how-to-generate-a-random-password-using-javascript/
  let password = '';
  const allChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$';

  for (let i = 1; i <= pwdlen; i += 1) {
    const char = Math.floor(Math.random() * allChars.length + 1);
    password += allChars.charAt(char);
  }

  return password;
}
