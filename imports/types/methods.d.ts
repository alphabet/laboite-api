import { NotificationType } from './collections';

type PostNotifContentType = Partial<NotificationType> & {
  email: string;
  username: string;
};

type ResponseAPIType = {
  code: number;
  status: string;
  message?: string;
  data?: any;
};
