declare module 'meteor/mongo' {
  import SimpleSchema from 'simpl-schema';
  namespace Mongo {
    export interface Collection<T> {
      schema: SimpleSchema;
      publicFields?: {
        [key: string]: 1;
      };
      allPublicFields?: {
        [key: string]: 1;
      };
      adminFields?: {
        [key: string]: 1;
      };
      typeLabels?: {
        [key: number]: string;
      };
      attachSchema(schema: SimpleSchema): void;
      attachJSONSchema(schema: any): void;
      helpers(methods: object): void;
      _name: string;
    }
  }
}
