import { Meteor } from 'meteor/meteor';
import { NotificationTypeType } from './enums/collections';

export type NotificationType = {
  userId?: string;
  groupId?: string;
  title?: string;
  content?: string;
  link?: string;
  createdAt?: Date;
  expireAt?: Date;
  type?: NotificationTypeType;
  read?: boolean;
};

export type GroupType = {
  _id: string;
  name: string;
  slug: string;
  description?: string;
  active: boolean;
  groupPadID?: string;
  digest?: string;
  type: string;
  avatar?: string;
  owner: string;
  numCandidates: number;
  plugins: any;
  articles: string;
  admins: string[];
  animators: string[];
  members: string[];
  candidates: string[];
  content?: string;
  applications?: string[];
  meeting?: {
    attendeePW: string;
    moderatorPW: string;
    createTime: string;
  };
};

export type UserType = Meteor.User & {
  firstName: string;
  lastName: string;
  lastLogin: Date;
  heartbeat: Date;
  isActive: boolean;
  isRequest: boolean;
  favServices: string[];
  favGroups: string[];
  favUserBookmarks: string[];
  primaryEmail: string;
  language: string;
  logoutType: string;
  advancedPersonalPage: boolean;
  articlesCount: number;
  lastArticle: Date;
  avatar: string;
  groupCount: number;
  groupQuota: number;
  mezigName: string;
  authToken: string;
  articlesEnable: boolean;
  structure: string;
};

export type IntroductionStructure = {
  language: string;
  title: string;
  content: string;
};

export type StructureType = {
  _id: string;
  name: string;
  parentId: string;
  childrenIds: string[];
  ancestorsIds: string[];
  introduction: IntroductionStructure[];
  contactEmail: string;
  groupId: string;
  asamExtensionsIds: string[];
  userStructureValidationMandatory: boolean;
  iconUrlImage: string;
  coverUrlImage: string;
  externalUrl: string;
  sendMailToParent: boolean;
  sendMailToStructureAdmin: boolean;
};

export type ArticleGroup = {
  _id: string;
  name: string;
  type: number;
};

export type ArticleType = {
  title: string;
  slug: string;
  userId: string;
  draft: boolean;
  structure: string;
  markdown: boolean;
  content: string;
  description: string;
  tags: string;
  groups: ArticleGroup[];
  createdAt: Date;
  updatedAt: Date;
  visits: number;
  licence: string;
};

export type BookmarksType = {
  url: string;
  name: string;
  author: string;
  groupId: string;
  tag: string;
  icon: string;
};

export type settingsGroupType = {
  _id: string;
  name: string;
  type: number;
};

export type settingsParticipantType = {
  _id: string;
  email: string;
  status: number;
  groupId: string;
};

export type EventType = {
  _id: string;
  title: string;
  location: string;
  recurrent: boolean;
  daysOfWeek: number[];
  description: string;
  start: Date;
  end: Date;
  startTime: string;
  endTime: string;
  allDay: boolean;
  groups: settingsGroupType[];
  participants: settingsParticipantType[];
  guests: string[];
  userId: string;
  createdAt: Date;
  updatedAt: Date;
};

export type ComponentType = {
  id: string;
  type: string;
  title: string;
  choices: string[];
};

export type AnswersType = {
  userId: string;
  createdAt: Date;
  modifyAnswersToken: string;
  answers: any[];
};

export type FormType = {
  title: string;
  description: string;
  isModel: boolean;
  createdAt: Date;
  owner: string;
  isPublic: boolean;
  active: boolean;
  editableAnswers: boolean;
  groups: string[];
  components: ComponentType[];
  formAnswers: AnswersType[];
};

export type SingleDateType = {
  date: Date;
  slots: string[];
};

export type SingleMeetingSlotType = {
  start: Date;
  end: Date;
};

export type PollType = {
  _id: string;
  title: string;
  userId: string;
  description: string;
  duration: string;
  public: boolean;
  allDay: boolean;
  active: boolean;
  completed: boolean;
  choosenDate: Date;
  type: string;
  groups: string[];
  dates: SingleDateType[];
  meetingSlots: SingleMeetingSlotType[];
  createdAt: Date;
  updatedAt: Date;
};

export type ServiceType = {
  title: string;
  slug: string;
  team: string;
  usage: string;
  content: string;
  description: string;
  url: string;
  logo: string;
  state: number;
  categories: string[];
  screenshots: string[];
  structure: string;
  offline: boolean;
};
