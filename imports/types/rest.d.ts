import { UserType } from './collections';

type Router = {
  [method: string]: {
    route: (path: string, callback: RouteFunction) => any;
    filter: () => any;
  };
};

type RestMethod = {
  method: string;
  path: string;
  auth?: boolean;
  roles?: string;
  func: RouteFunction;
  route: string;
  swagger: any;
};
type RouteFunction = (params?: any, req?: any, res?: any, next?: any) => void;

type RestResponse = {
  code: number;
  status: string;
  message: string;
  data?: {
    [key: string]: any;
  };
};

type WithParameters = {
  parameters: any;
};

type RestRequest = Request & WithParameters;

export type RequestFunctionType = (
  request: RestRequest,
  content: any,
  pathname: string,
  path: string,
  sender?: Partial<UserType> & { _id: string },
) => void;

export type RestRouteDefType = {
  route: string;
  method: string;
  auth: boolean;
  func: RequestFunctionType;
  path: string;
  swagger: any;
};
