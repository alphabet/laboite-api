# The DEV environment **Laboite-API** application :

- [Install](#install)
  - [Application : Laboite](#application-laboite)
  - [Application : Laboite-API](#application--laboite-api)
  - [Parameters](#parameters)
- [Run project](#run-project)
  - [In a terminal **laboite**](#in-a-terminal-laboite)
  - [Run an other terminal **Laboite-API**](#run-an-other-terminal-laboite-api)
  - [Add groups to your user](#create-your-user)
    - [In user interface **localhost:3000**](#in-user-interface-localhost3000)
  - [Authenticate on Laboite-API](#authenticate-on-laboite-api)

---

## Install

### Application : Laboite

Install process :

```
git clone https://gitlab.mim-libre.fr/alphabet/laboite.git
cd laboite
cp config/settings.development.json.sample config/settings.development.json
cd app
npm install
```

### Application : Laboite-API

Install process :

```
git clone https://gitlab.mim-libre.fr/alphabet/laboite-api.git
cd laboite-api
npm install
```

### Parameters

To run **Laboite-API** locally, you need to configure a **LaBoite**' local instance with authentication on a Keycloak server.

## Run project

### In a terminal **laboite**

```
cd laboite/app
meteor npm start
```

It is possible to check the operation of the box by typing the following line from an web browser

```
http://localhost:3000
```

### Run an other terminal **Laboite-API**

```
cd laboite-api
npm start
```

From the browser, type this :

```
http://localhost:3030
```

### Create your user

#### In user interface **localhost:3000**

From the `LaBoite` app that you access from the browser

```
http://localhost:3000
```

Go to the config file in LaBoite `./config/settings.development.json`

Change the attribute : "whiteDomains" according to your mail provider.

Exemple :

For mailUser = 'toto@gmail.com', you must add "^gmail.com"

which would give :

    "whiteDomains": [
      "^ac-[a-z-]\\.fr",
      "^[a-z-]\\.gouv.fr",
      "^gmail.com"
    ]

Re run la boite

Go on `http://localhost:3000` (create your user in keycloak by following the link on the authentication page).

### Authenticate on Laboite-API

navigate to `http://localhost:3000`

In the dropdown menu (top right corner), click on "profile" and scroll to the bottom of the profile page.

Open the "Get the Access Token" panel, and click on "Get access token"

Navigate to `http://localhost:3030`

Click on the "Authorize" button, and paste your token (ctrl + v) in the value field.
Click on "Authorize" (green button), then on "close".

you can now browse the various available APIs, and try them with the "Try it out" button, then "Execute".
