# Changelog

### [1.0.2](https://gitlab.mim-libre.fr/alphabet/laboite-api/compare/release/1.0.1...release/1.0.2) (2024-06-17)


### Bug Fixes

* **libraries:** update vulnerable libraries and meteor version ([d1e64c3](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/d1e64c3a9a761edc14a407f800b7adcc2573f782))

### [1.0.1](https://gitlab.mim-libre.fr/alphabet/laboite-api/compare/release/1.0.0...release/1.0.1) (2024-01-30)


### Bug Fixes

* **audit:** update vulnerable libraries ([a42aead](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/a42aead569bee2d502749e6d79d69c2dc3c105cb))

## 1.0.0 (2023-11-14)


### Features

* **api:** remove notifications API endpoints ([83a3650](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/83a3650ffeccea6cdf986e203952961dd708c970))
* **auth:** add authtoken instead of keycloak token ([66d9aef](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/66d9aef175a22b21f4d8acac22a075b2916a2bb8))
* **auth:** add authtoken instead of keycloak token ([0cc2c81](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/0cc2c814941b041ad5f772e638176a9ac81df6c0))
* **libs:** update ooutdated libs to wanted version ([2010984](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/2010984b488a98120c76b9482d7602bc60c53b30))
* **notif:** add email in userId to notifications post ([ef8625d](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/ef8625d4f55250a5bd7a21e3e9dab40ff1d66fce))
* **notif:** add email in userId to notifications post ([11d1007](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/11d100763d555ae8bc0c67f46d32cc5ad350bf41))
* **notifications:** add current user api ([7a21878](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/7a2187836b23d4e42d2ee6002326a246990ee292))
* **notifications:** add current user api ([9de9ee8](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/9de9ee841ba2c488f39d5cc321c331815303723e))
* **statistics:** add estimated active users per month ([a1823bc](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/a1823bcab55685aae0c4ffb7521327e7be752246))
* **statistics:** add global users and monthly users statistics ([efcdc9e](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/efcdc9e7acb3f246e35b277552bf5a51306432b6))
* **statistics:** add mail domain parameter to users stats ([7e7bfeb](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/7e7bfebfe64b8ebb4ba0c9546050f5267d232bd8))
* **statistics:** add parameter for number of months (monthly users) ([5235d21](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/5235d21d984ef10948cec886037ecfadbc19aa68))
* **statistics:** add structureId parameter to user statistics ([acdabb3](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/acdabb3d9686d29987747823ebf353d003a893d2))
* **statistics:** more detailed statistics for single group ([1f344c0](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/1f344c0046a595ed910c3d98561575ff455cb5d5))
* **stats:** refactor and add some basic statistics routes ([4c6285a](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/4c6285a623d067c049e13cdf24ce04d4ef398eb0))
* **structure:** add services and structures to api ([acee346](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/acee346062e9f0e9da35988693747f9b13364a61))
* **structure:** add services and structures to api ([e090403](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/e090403a48117c8ffe76fcfda0b5bcc4ebaba96c))


### Bug Fixes

* **accounts:** disable client account creation ([d348261](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/d348261d72273164affacf6e02d379aa1136c742))
* **ci:** disable husky hooks for CI, fix husky installation ([246cd1f](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/246cd1f1d1fb7ab67d95314fc04bdf11254d1c0a))
* **libs:** update vulnerable libraries ([52c1d87](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/52c1d87607afaafb75d09085cf924fe66dadd483))
* **notif:** ensure that userid is used if email is present ([db8a781](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/db8a781d0c3bad05caac6cf65c5ab84c6a479b56))
* **notif:** ensure that userid is used if email is present ([38298ee](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/38298ee80160a0ba8686c936d60145b040d8fa5f))
* **notifs:** fix/notifications/me ([5621266](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/56212663bf73708eb303d6f6205a929fd520225d))
* **notifs:** fix/notifications/me ([fd43f1d](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/fd43f1db31678995d61f4d1819b7f3d52dd35afa))
* **packages:** fix start script for running locally ([21cd005](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/21cd0050eaf7a50f5309d3018399a7de09278a42))
* **statistics:** fix entry for users with no structure ([c0c7bda](https://gitlab.mim-libre.fr/alphabet/laboite-api/commit/c0c7bda0290e88c121e30ec51309ef8b3943cefe))
