# L'application **Laboite-API** environnement DEV :

- [Installation](#installation)
  - [Application : Laboite](#application-laboite)
  - [Application : Laboite-API](#application-laboite-api)
  - [Paramètres](#paramètres)
- [Lancer le projet](#lancer-le-projet)
  - [Dans un terminal **Laboite**](#dans-un-terminal-laboite)
  - [Lancer un autre terminal **Laboite-API**](#lancer-un-autre-terminal-laboite-api)
  - [Créer votre utilisateur](#créer-votre-utilisateur)
    - [Via l'interface utilisateur **localhost:3000**](#via-linterface-utilisateur-localhost3000)
  - [Se connecter sur Laboite-API](#se-connecter-sur-laboite-api)

---

## Installation

### Application : Laboite

Procédure d'installation :

```
git clone https://gitlab.mim-libre.fr/alphabet/laboite.git
cd laboite
cp config/settings.development.json.sample config/settings.development.json
cd app
npm install
```

### Application : Laboite-API

Procédure d'installation :

```
git clone https://gitlab.mim-libre.fr/alphabet/laboite-api.git
cd laboite-api
npm install
```

### Paramètres

Pour le fonctionnement de **Laboite-API** en local, il faut configurer une instance locale de **LaBoite** avec authentification sur un serveur Keycloak.

## Lancer le projet

### Dans un terminal **Laboite**

```
cd laboite/app
npm start
```

Il est possible de vérifier le fonctionnement de la boite en tapant la ligne suivante à partir d'un navigateur :

```
http://localhost:3000
```

### Lancer un autre terminal **Laboite-API**

```
cd laboite-api
npm start
```

À partir du navigateur, tapez ceci :

```
http://localhost:3030
```

### Créer votre utilisateur

#### Via l'interface utilisateur **localhost:3000**

À partir de l'application `LaBoite` que vous accédez à partir du navigateur

```
http://localhost:3000
```

Aller dans le fichier de config de Laboite `./config/settings.development.json`

Modifier l'attribut : "whiteDomains" en fonction de votre mail user

Exemple :

Pour un mailUser = 'toto@gmail.com', il faudra ajouter "^gmail.com"

Ce qui donnerait :

    "whiteDomains": [
      "^ac-[a-z-]\\.fr",
      "^[a-z-]\\.gouv.fr",
      "^gmail.com"
    ]

Relancer la boite

Naviguez sur `http://localhost:3000` (créez votre utilisateur dans keycloak en suivant le lien proposé sur la page d'authentification).

### Se connecter sur Laboite-API

Naviguez sur `http://localhost:3000`

Dans le menu déroulant utilisateur (en haut à droite), cliquez sur "Profil", et allez tout en bas de la page

déroulez le panneau "Récupérer le token d'accès", et cliquez sur "Récupérer le token"

Naviguez sur `http://localhost:3030`

Cliquez sur le bouton "Authorize" et coller votre jeton (ctrl + v) dans le champ value.
Cliquez sur "Authorize" (bouton vert), puis sur "close"

Vous pouvez maintenant dérouler les différentes API disponibles, et les essayer avec le bouton "Try it out", puis "Execute".
